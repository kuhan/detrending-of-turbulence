function [vel_bin,pwr_bin,err_bin,unc,nc] = bindata(spd,pwr,xo,xd,xm,mo)
%
%  Binning of data e.q. power measurements or turbulence vs wind speed
%  Input:   spd = wind speed
%           pwr = power
%           bin size: bin = [xd/2:xd:xm]
%           mo = minimum accepted number of observation in each bin
%           (def=0)
%  Output:  vel_bin = averaged wind speed in bin
%           pwr_bin = average power in bin
%           err_bin = stdev (power) in bin
%           unc     = standard bin-uncernaty (=stdev/sqrt(nc))
%           nc      = number of count in bin
%           version: 17/3-2015 / kuhan@dtu.dk
%           modif  : 21/11-2016/ kuhan@dtu.dk
%           mo=xo; 
if mo>0, min1=mo; else min1=0; end;
bin = [xo:xd:xm+xd/2];
for i = 1:length(bin)
    ind = find(spd>=(bin(i)-xd/2) & spd<(bin(i)+xd/2)); % 0.5m/s bins
    pts_bin(i) = length(ind);  % bin nb of points
    if isempty(ind)
        pwr_bin(i) = NaN; vel_bin(i) = NaN; err_bin(i) = NaN; nc(i)=NaN; unc(i)=NaN;
    else
        pwr_bin(i) = mean(pwr(ind)); % bin average power        
        vel_bin(i) = bin(i); % bin average speed
        err_bin(i) = std(pwr(ind)); % bin standard deviation
        nc(i)      = max(size(pwr(ind))); %number of counts
        unc(i)     = std(pwr(ind))/sqrt(max(size(pwr(ind))));
        if nc(i)<min1, pwr_bin(i) = NaN; vel_bin(i) = NaN; err_bin(i) = NaN; nc(i)=NaN; unc(i)=NaN; end;
    end
end
pwr_bin=pwr_bin'; pwr_bin(isnan(pwr_bin(:,1)))=[];
vel_bin=vel_bin'; vel_bin(isnan(vel_bin(:,1)))=[];
err_bin=err_bin'; err_bin(isnan(err_bin(:,1)))=[];
nc=nc'; nc(isnan(nc(:,1)))=[];
unc=unc'; unc(isnan(unc(:,1)))=[];
end