# De-trending of wind speed variance

Readme file for DE_Trend tool version 1.0.0.0
Version: 4/6-2018 

Reference: Gunner Chr. Larsen and Kurt S. Hansen; 
De-trending of wind speed variance based on first-order and second-order 
statistical moments only. 
[Wind Energ. 2013, DOI: 10.1002/we.1676](https://doi.org/10.1002/we.1676)

Syntax for setup file = DT_setup.txt
====================================
line 1: Infomation 
line 2: inputfile with data to be de-trended stored as csv file with ",' as seperator
Lile 3: cols, time_index, time_index_type, windspeed, STDV  where
cols=number of columns in datafile;
time_index= columns with time index for the recordings;
time_index_type=1 => increasing sequence, starting from 1,...,N; note missing or faulty recordings should be remove. 
		     The routine selects only groups of consecutive recordings (3 observations in each group) for processing. 
time_index_type=10=> increasing runname for data recording; format = yyyymodahhmi, yyyy=year(4-digits), mo=month(2 digits), 
		     da=day(2 digits), hh=hour(2 digits) and mi=minute (2 digits). The tool selects only consecutive recordings 
		     within+/- 10 minutes for processing.  
windspeed=mean wind speed [m/s] for 10-minute;
STDV= standard deviation of wind speed [m/s] for 10 -minute;
ti= Turbulence intensity [%] = standard deviation/mean wind speed;

Test data extracted from winddata.com with reference detrending, which has been performed on the raw time series.

Example 1:
========
DE_Trend setup file version 1.0 for site= Skiheya, 72m, see description in readme.txt

ski_data_72m.csv

1 10 3 4 5 1 cols: time_index, time_index_type, ws, stdev, iplot 

Example 2:
========
DE_Trend setup file version 1.0 for site= Gunfleet Sands, see description in readme.txt
Gunfleet_Sands_2003.csv
1 10 2 3 4 1 cols: time_index, time_index_type, ws, stdev, iplot 

Example 3:
========
DE_Trend setup file version 1.0 for site= Skiheya, 46m, see description in readme.txt
sle_data_46m.csv
2 1 4 5 6 1 cols: time_index, time_index_type, ws, stdev, iplot 


