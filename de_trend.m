% Function: matriks
% =================
% Determination of speed and gradient using 4 order Nat. cubic splines

% Reference: 
% Gunner Chr. Larsen and Kurt S. Hansen; De-trending of wind speed variance based on first-order and second-order
% statistical moments only. Wind Energ. 2013, DOI: 10.1002/we.1676

% System 8 x 8 matrix, P''=0, p'(i)=P'(i-1)
% Input: vm[1:3]=>> V for t = i-1,i,i+1
%
% Option ig=4 => ALGORITM 1: integralbetingelse + P1(ti)=P2(ti) & P1''(ti)=P2''(ti)
% Matrix(15x15) based on first order statistical moments
%
% Option ig=6 => ALGORITM 2: integralbetingelse + P1(ti)=P2(ti) & P1''(ti)=P2''(ti)
% Matrix(12x12) based on second order statistical moments
%
% Public version 1.0; Function: matriks 
% Date:         30-07-2019
% Author:       kuhan@dtu.dk 
%
function [v,k,as,vs,A,b,k2,C,C2,nc,hkk,keps] = matriks(vm,st,i,ig,C2g);
as=(1.:.1:2.);as=as';[ns n2]=size(as);
vs=zeros(ns,1);
%
% ig = 4 >> ALGORITM 1:integralbetingelse + P1(ti)=P2(ti) & P1''(ti)=P2''(ti) 
% Matrix(15x15)
if ig==4,
a=[0:1:3];
b=zeros(15,1); A=zeros(15,15);
    % 1.row - P1() integral betingelse
    k=1; h=2;
     A(k,1)=(a(h)^5-a(h-1)^5)/5;   
     A(k,2)=(a(h)^4-a(h-1)^4)/4;   
     A(k,3)=(a(h)^3-a(h-1)^3)/3;   
     A(k,4)=(a(h)^2-a(h-1)^2)/2;        
     A(k,5)=(a(h)-a(h-1));  
     b(k)=vm(i-1);
     % 2.row - P2() integral betingelse
    k=2; h=3;
     A(k,6)=(a(h)^5-a(h-1)^5)/5;   
     A(k,7)=(a(h)^4-a(h-1)^4)/4;   
     A(k,8)=(a(h)^3-a(h-1)^3)/3;   
     A(k,9)=(a(h)^2-a(h-1)^2)/2;        
     A(k,10)=(a(h)-a(h-1));   
     b(k)=vm(i);
     % 3.row - P3() integral betingelse
    k=3; h=4;
     A(k,11)=(a(h)^5-a(h-1)^5)/5;   
     A(k,12)=(a(h)^4-a(h-1)^4)/4;   
     A(k,13)=(a(h)^3-a(h-1)^3)/3;   
     A(k,14)=(a(h)^2-a(h-1)^2)/2;        
     A(k,15)=(a(h)-a(h-1));   
     b(k)=vm(i+1);
     % 4. row; P1(i)=P2(i)
     k=4; h=2;
     for l=1:5, A(k,l)=a(h)^(5-l); A(k,l+5)=-a(h)^(5-l); end;
     % 5. row; P2(i+1)=P3(i+1)
     k=5; h=3;
     for l=1:5, A(k,l+5)=a(h)^(5-l); A(k,l+10)=-a(h)^(5-l); end;
     % 6. row; P1'(i)=P2'(i)
     k=6; h=2;
     for l=1:4, A(k,l)=(5-l)*a(h)^(4-l); A(k,l+5)=-(5-l)*a(h)^(4-l); end;
     % 7. row; P2'(i+1)=P3'(i+1)
     k=7; h=3;
     for l=1:4, A(k,l+5)=(5-l)*a(h)^(4-l); A(k,l+10)=-(5-l)*a(h)^(4-l); end;
     % 8. row; P1''(i)=P2''(i)
     k=8; h=2;
     for l=1:3, A(k,l)=(5-l)*(4-l)*a(h)^(3-l); A(k,l+5)=-(5-l)*(4-l)*a(h)^(3-l); end;
     % 9. row; P2''(i+1)=P3''(i+1)
     k=9; h=3;
     for l=1:3, A(k,l+5)=(5-l)*(4-l)*a(h)^(3-l); A(k,l+10)=-(5-l)*(4-l)*a(h)^(3-l); end;     
     % 10. row; P1'''(i)=P2'''(i)
     k=10; h=2;
     for l=1:2, A(k,l)=(5-l)*(4-l)*(3-l)*a(h)^(2-l); A(k,l+5)=-(5-l)*(4-l)*(3-l)*a(h)^(2-l); end;
     % 11. row; P2''(i+1)=P3''(i+1)
     k=11; h=3;
     for l=1:3, A(k,l+5)=(5-l)*(4-l)*(3-l)*a(h)^(2-l); A(k,l+10)=-(5-l)*(4-l)*(3-l)*a(h)^(2-l); end;
     % 12. row; P1''(i-1)=0
     k=12; h=1;
     for l=1:3, A(k,l)=(5-l)*(4-l)*a(h)^(3-l);  end;
     % 13. row; P1'''(i-1)=0
     k=13; h=1;
     for l=1:2, A(k,l)=(5-l)*(4-l)*(3-l)*a(h)^(2-l);  end;
     % 14. row; P3''(i+2)=0
     k=14; h=4;
     for l=1:3, A(k,l+10)=(5-l)*(4-l)*a(h)^(3-l);  end;
     % 15. row; P3'''(i+1)=0
     k=15; h=4;
     for l=1:2, A(k,l+10)=(5-l)*(4-l)*(3-l)*a(h)^(2-l);  end;
     det(A);
    P=A\b; x=1;
    v=P(6)*as(6)^4+P(7)*as(6)^3+P(8)*as(6)^2+P(9)*as(6)+P(10);
    k=12*(P(6)*63/6+P(7)*31/5+ P(8)*15/4+P(9)*7/3+P(10)*3/2-3*b(2)/2); 
    if imag(k) ~= 0,
        P
        v
        k
    end;
    for l=1:ns, vs(l)=P(6)*as(l)^4+P(7)*as(l)^3+P(8)*as(l)^2+P(9)*as(l)+P(10); end; 
end;
%
% ig = 6 >> ALGORITM 2: integralbetingelse + P1(ti)=P2(ti) & P1''(ti)=P2''(ti) 
% Matrix(12x12)
% 9-6-2008 Modif with sign of slope
if ig==6,
a=[0:1:3];
b=zeros(14,1); A=zeros(14,12); C2=ones(1,3);hs=ones(1,3);
k1=zeros(3,1); k2=ones(3,1);  C=zeros(1,3);sigL=zeros(1,3);C1=zeros(1,3);
    % 1.row - P1() integral betingelse
    k=1; h=2; 
     A(k,1)=(a(h)^4-a(h-1)^4)/4;   
     A(k,2)=(a(h)^3-a(h-1)^3)/3;   
     A(k,3)=(a(h)^2-a(h-1)^2)/2;        
     A(k,4)=(a(h)-a(h-1));  
     b(k)=vm(i-1);
     % 2.row - P2() integral betingelse
    k=2; h=3;
     A(k,5)=(a(h)^4-a(h-1)^4)/4;   
     A(k,6)=(a(h)^3-a(h-1)^3)/3;   
     A(k,7)=(a(h)^2-a(h-1)^2)/2;        
     A(k,8)=(a(h)-a(h-1));   
     b(k)=vm(i);
     % 3.row - P3() integral betingelse
    k=3; h=4;
     A(k,9)=(a(h)^4-a(h-1)^4)/4;   
     A(k,10)=(a(h)^3-a(h-1)^3)/3;   
     A(k,11)=(a(h)^2-a(h-1)^2)/2;        
     A(k,12)=(a(h)-a(h-1));   
     b(k)=vm(i+1);
     % 4. row; P1(i)=P2(i)
     k=4; h=2;
     for l=1:4, A(k,l)=-a(h)^(4-l); A(k,l+4)=a(h)^(4-l); end;
     % 5. row; P2(i+1)=P3(i+1)
     k=5; h=3;
     for l=1:4, A(k,l+4)=-a(h)^(4-l); A(k,l+8)=a(h)^(4-l); end;
     % 6. row; P1'(i)=P2'(i)
     k=6; h=2;
     for l=1:3, A(k,l)=-(4-l)*a(h)^(3-l); A(k,l+4)=(4-l)*a(h)^(3-l); end;
     % 7. row; P2'(i+1)=P3'(i+1)
     k=7; h=3;
     for l=1:3, A(k,l+4)=-(4-l)*a(h)^(3-l); A(k,l+8)=(4-l)*a(h)^(3-l); end;
     % 8. row; P1''(i)=P2''(i)
     k=8; h=2;
     for l=1:2, A(k,l)=-(4-l)*(3-l)*a(h)^(2-l); A(k,l+4)=(4-l)*(3-l)*a(h)^(2-l); end;
     % 9. row; P2''(i+1)=P3''(i+1)
     k=9; h=3;
     for l=1:2, A(k,l+4)=-(4-l)*(3-l)*a(h)^(2-l); A(k,l+8)=(4-l)*(3-l)*a(h)^(2-l); end;     
     % 10. row; P1''(i-1)=0
     k=10; h=1;
     A(10,2)=2;
     % 11. row; P3''(i+2)=0
     k=11; h=4;
     A(11,9)=18;     
     A(11,10)=2;
     % 12. row; h(i-1)=f(sig(i-1),v(i-1));P1
     k=12; h=1;
     A(k,1)=(1+5*a(h)+10*a(h)^2+10*a(h)^3+5*a(h)^4)/5;
     A(k,2)=(1+4*a(h)+6*a(h)^2+4*a(h)^3)/4;
     A(k,3)=(1+3*a(h)+3*a(h)^2)/3;
     A(k,4)=(1+2*a(h))/2;
     % 13. row; h(i)=f(sig(i),v(i));P2
     k=13; h=2;
     A(k,5)=(1+5*a(h)+10*a(h)^2+10*a(h)^3+5*a(h)^4)/5;
     A(k,6)=(1+4*a(h)+6*a(h)^2+4*a(h)^3)/4;
     A(k,7)=(1+3*a(h)+3*a(h)^2)/3;
     A(k,8)=(1+2*a(h))/2;
     % 14. row; h(i+1)=f(sig(i+1),v(i+1));P3
     k=14; h=3;
     A(k,9)=(1+5*a(h)+10*a(h)^2+10*a(h)^3+5*a(h)^4)/5;
     A(k,10)=(1+4*a(h)+6*a(h)^2+4*a(h)^3)/4;
     A(k,11)=(1+3*a(h)+3*a(h)^2)/3;
     A(k,12)=(1+2*a(h))/2;
     k1=[.1,.1,.1];
    j=0;eps=1;
    sigL(1)=(st(i-1)/vm(i-1))^2;
    sigL(2)=(st(i)/vm(i))^2;
    sigL(3)=(st(i+1)/vm(i+1))^2;
      C2a=  C2g; nci=0;
      if C2a > min(sigL), 
          C2a=0.96*min(sigL);           
      end;
      %C2a=0.0225;
      hs(1)=sign(vm(i)-vm(i-2))*hs(1);
      hs(2)=sign(vm(i+1)-vm(i-1))*hs(2);
      hs(3)=sign(vm(i+2)-vm(i))*hs(3);  
      Yk=sqrt(1/12);
      j=1;
      for h=1:4, hkk(h,j)=sqrt(C2a); end;
      keps(1)=0;
      %for h=1:3, if hs(h)==0, hs(h)=1; end; end;
        % stop criteria: j>= 200 or (1+eps)>1.000001
      while (j<200) & ((1+eps)>1.00001), 
         %hs
      b(12)=hs(1)*Yk*sqrt(st(i-1)^2-C2a*vm(i-1)^2)+(a(1)+0.5)*vm(i-1);
    b(13)=hs(2)*Yk*sqrt(st(i)^2-C2a*vm(i)^2)+(a(2)+0.5)*vm(i)  ;
    b(14)=hs(3)*Yk*sqrt(st(i+1)^2-C2a*vm(i+1)^2)+(a(3)+0.5)*vm(i+1);    
    %if imag(b(14)) ~= 0,  b ; C2a ;  end;
    P=pinv(A)*b;
     %P=pinv(A)*real(b);
    h=1; k2(h)=  12*P(1)*(1+5*a(h)+10*a(h)^2+10*a(h)^3+5*a(h)^4)/5;
         k2(h)= k2(h)+12*P(2)*(1+4*a(h)+6*a(h)^2+4*a(h)^3)/4;
         k2(h)= k2(h)+12*P(3)*(1+3*a(h)+3*a(h)^2)/3;
         k2(h)= k2(h)+12*P(4)*(1+2*a(h))/2 -12*(a(h)+0.5)*b(h);
         C1(h)=sqrt(st(i-1)^2 - k2(h)^2/12)/vm(i-1);         
         if abs(k2(h))>(st(i-1)*sqrt(12)), k2(h)=sign(k2(h))*0.98*st(i-1)/Yk; end;
         hs(h)=sign(k2(h)); 
    h=2; k2(h)=       12*P(5)*(1+5*a(h)+10*a(h)^2+10*a(h)^3+5*a(h)^4)/5;
         k2(h)= k2(h)+12*P(6)*(1+4*a(h)+6*a(h)^2+4*a(h)^3)/4;
         k2(h)= k2(h)+12*P(7)*(1+3*a(h)+3*a(h)^2)/3;
         k2(h)= k2(h)+12*P(8)*(1+2*a(h))/2-12*(a(h)+0.5)*b(h);      
         C1(h)=sqrt(st(i)^2 - k2(h)^2/12)/vm(i);        
         if abs(k2(h))>(st(i)*sqrt(12)), k2(h)=sign(k2(h))*0.98*st(i)/Yk; end;
         hs(h)=sign(k2(h)); 
    h=3; k2(h)=       12*P(9)*(1+5*a(h)+10*a(h)^2+10*a(h)^3+5*a(h)^4)/5;
         k2(h)= k2(h)+12*P(10)*(1+4*a(h)+6*a(h)^2+4*a(h)^3)/4;
         k2(h)= k2(h)+12*P(11)*(1+3*a(h)+3*a(h)^2)/3;
         k2(h)= k2(h)+12*P(12)*(1+2*a(h))/2-12*(a(h)+0.5)*b(h); 
         if abs(k2(h))>(st(i+1)*sqrt(12)), k2(h)=sign(k2(h))*0.98*st(i+1)/Yk; end;
         C1(h)=sqrt(st(i+1)^2 - k2(h)^2/12)/vm(i+1);
         if imag(C1(1)) ~= 0, C1(1)=st(i-1)/vm(i-1); end;
         if imag(C1(2)) ~= 0, C1(2)=st(i)/vm(i); end;
         if imag(C1(3)) ~= 0, C1(3)=st(i+1)/vm(i+1); end;
         %k2  C2 hs
         hs(h)=sign(k2(h)); 
         if imag(C1) ~= 0, nci=nci+1;  end;
    j=j+1;   
    for h=1:3, 
        C2(h)=C1(h)^2; end;
    eps=((mean(C1)-mean(C))/mean(C1))^2;
    for h=1:3, eps=eps+((k2(h)-k1(h))/k2(h))^2+((C1(h)-C(h))/C1(h))^2; end
    keps(j)=eps;
    k1=k2;
    C=C1;
    C2a=mean(C1)^2;  %midling
    hkk(4,j)=0;
    for h=1:3, hkk(h,j)=C1(h); hkk(4,j)=hkk(4,j)+(C1(h)-mean(C1))^2/mean(C1)^2; end; 
        if C2a > min(sigL) , 
        C2a=min(sigL);
        end;
    end;
    %eps
    nc=j;
    %P=real(P);
    v=P(5)*as(6)^3+P(6)*as(6)^2+P(7)*as(6)+P(8);   
    k=12*(P(5)*31/5+P(6)*15/4+ P(7)*7/3+P(8)*3/2-3*b(2)/2); 
    for l=1:ns, vs(l)=P(5)*as(l)^3+P(6)*as(l)^2+P(7)*as(l)+P(8); end; 
    if nci>0, 
        nci
    end;
end;