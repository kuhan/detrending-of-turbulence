clc
close all
clear all
% Detrending wind speed variance

% Public version 1.0 -- main program
% Date:         30-07-2019
% Author:       kuhan@dtu.dk 

% Input: rn,ws,stdv,ti,tik,wd, iplot
% ==================================
%   runname or index(=rid)
%   ws: wind speed [m/s]
%   stdv: standard deviation
%   ti: turbulence intensity (=100*stdev/ws);
%   tik: detrended turbulence intensity (based on time series) - optional
%   wd: wind direction - optional
%   iplot: =1 => plot of ws & ti signals and bin-averaged tic

% Output: rid, ws, ti, tic, 

% Reference method: 
% Gunner Chr. Larsen and Kurt S. Hansen; De-trending of wind speed variance based on first-order and second-order
% statistical moments only. Wind Energ. 2013, DOI: 10.1002/we.1676
% 8-3-2012 Final version used for test & detrending data

% Used functions: de_trend.m & bindata.m


iplot=0;
fprintf(1,' Program: DTrend version 1.0,                    \n');
fprintf(1,' (c) Kurt S. Hansen DTU Wind Energy; kuhan@dtu.dk\n');
fprintf(1,' ==================================30th July 2019===\n\n');

loaddir = 'Testdata/';

%fn_out=['DT_output.csv'];
fid=fopen([loaddir,'DT_setup.txt'],'r');
st1=fgetl(fid);
fn=fgetl(fid);
fprintf('Input file  = %s\n',fn);
c=fscanf(fid,'%d');c2=c';
id=c(1); idt=c(2); iw=c(3); it=c(4); it2=c(5);
fprintf(1,'Input codes = %d %d %d %d \n',id,idt,iw,it);
if length(c)>5, iplot=c(6); end
fclose(fid);
x=csvread([loaddir,fn],1,0);
[nr,nc1]=size(x);
if idt==1, rid=x(:,id); end
if idt==10,  formatIn = 'yyyymmddHHMM';
            dtime=datenum(int2str(int64(x(:,id))),formatIn);
            dt0=dtime(1); rid(1)=1;
 for j=2:nr 
     dmin=144*(dtime(j)-dtime(j-1)); rid(j)=rid(j-1)+int64(dmin);
 end
end
ws=x(:,iw); 
sig=x(:,it);
%ti=x(:,it2);
ti=sig./ws; X = find(isnan(ti)); ti(X)=0; 
tic=ti;
ns=max(size(ws));
fprintf(1,'Records  =  %d\n',ns); 
fn_out=strrep(fn,'.csv','_o.csv'); fod=fopen(fn_out,'w');
fprintf(1,'Output file=%s\n',fn_out);
if iplot==1, 
tmax=ns/6;
t=[1/6:1/6:tmax];
newChr = strrep(fn,'_','-');
fn_out=strrep(fn,'.csv','_o.csv'); fod=fopen(fn_out,'w');
tit=['DE-trending of measured turbulence; input file=',newChr,', size:',int2str(fix(tmax)),' hours']; 
subplot(4,1,1)
plot(t,ws,'-');
title(tit,'fontsize',16);
set(gca,'fontsize',14);
grid;
xlabel(' Hours ','fontsize',14);
ylabel('Measured wind speed - m/s','fontsize',14);
subplot(4,1,2)
plot(t,100*ti,'-');
grid;
xlabel(' Hours ','fontsize',14);
ylabel('Measured turbulence - %','fontsize',14);
ylim([0 50])
set(gca,'fontsize',14);
end;
%if iplot==1, [ mean(ws) mean(sig) 100*mean(ti)]  
%end;


res=zeros(1,1);

l=0;
%ns=9999;
for i=2:ns-1,
       %
    % Only consecutive measurements are detrended
    %
    if rid(i+1)-rid(i-1)==2, nc=0;
    C2g=0.95^2*(sig(i)/ws(i))^2; 
    %C2g=0.95*sig(i)/ws(i);
    %tic(i)=ti(i);
    if (i>2) & (i<ns-2) & (ws(i)>0.2) ,
    %
    % Default method 
    % ig=6; based on second-order statistical moments
    % ===============================================
    [v,k,as,vs,A,b,k2,C,C2,nc,hkk,keps]=de_trend(ws,sig,i,6,C2g); 
    t1=1; if isnan(C2(2))==1, t1=0; end;
     if (nc<199) & (ti(i)>=abs(C2(2))) & (ws(i)>.2) & t1==1,
          l=l+1;
          tic(i)=real(sqrt(C2(2)));
          ncc(i)=nc;
                           end;
    
    if mod(i,7*144)==0, 
              fprintf(1,'Working....valid=%d of %d/%d\n',l,i,ns)
              end;
  % [rid(i),ws(i),sig(i),ti(i),tic(i),ti(i)-tic(i),nc]
    end;
  end;
end;
tic=tic';ns1=ns-1;
tim=100*mean(ti(1:ns1)); ticm=100*mean(tic(1:ns1));
%if iplot==1, [mean(ws(1:ns1)) mean(sig(1:ns1)) tim ticm] 
%end;

if iplot==1, subplot(2,2,3)
    plot(100*ti(1:ns1),100*tic(1:ns1),'o','markersize',5); hold on;
    z=[1,1;50,50];
    plot(z,z,'g--');
    xlabel('Measured turbulence - %','fontsize',14);
    ylabel('De-trended turbulence - %','fontsize',14);
    axis([0 50 0 50]);
    grid; 
    tx2=['Processed data: ',int2str(ns/6),' hours'];
    legend(tx2,'location','northwest');
    set(gca,'fontsize',14);
    subplot(2,2,4)
    [ws1,tib,tis]=bindata(ws,100*ti,2,.5,25,3);
    [ws2,ticb,tics]=bindata(ws,100*tic,2,.5,25,3);
    errorbar(ws1,tib,tis,'o-'); hold on
    errorbar(ws2,ticb,tics,'s-');    
    xlabel('Wind speed - m/s','fontsize',14);
    ylabel('Turbulence - %','fontsize',14);
    txm=['Measured turbulence, <ti>=',num2str(tim,3),'%'];
    txc=['Detrended turbulence, <tic>=',num2str(ticm,3),'%'];
    legend(txm,txc,'fontsize',14);
    grid;    
    set(gca,'fontsize',14);
    end;
fprintf(fod,'rid,ws,sig,ti,tic,Input file=,%s\n',fn);
for j=1:ns,
    %fprintf(fod,'%d,%6.2f,%6.2f,%6.2f,%6.2f,%6.2f \n',rid(j),ws(j),sig(j),100*ti(j),100*tic(j),100*(ti(j)-tic(j)));
    fprintf(fod,'%d,%6.2f,%6.2f,%6.2f,%6.2f\n',rid(j),ws(j),sig(j),100*ti(j),100*tic(j));
end;    
fprintf(1,'.................... \n');
fprintf(1,'Results: <ti> =%6.2f \n',tim);  
fprintf(1,'Results: <tic>=%6.2f \n',ticm); 
fprintf(1,'Based on %d recordings\n',ns); 
fclose('all');
if iplot==1, orient landscape; end;